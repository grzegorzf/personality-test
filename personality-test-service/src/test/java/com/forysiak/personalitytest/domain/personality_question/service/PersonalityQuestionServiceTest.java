package com.forysiak.personalitytest.domain.personality_question.service;

import com.forysiak.personalitytest.PersonalityTestServiceApplication;
import com.forysiak.personalitytest.domain.personality_question.dao.PersonalityQuestionDAO;
import com.forysiak.personalitytest.domain.personality_question.model.answer.Answer;
import com.forysiak.personalitytest.domain.personality_question.model.answer.AnswerType;
import com.forysiak.personalitytest.domain.personality_question.model.question.*;
import com.forysiak.personalitytest.domain.personality_question.repository.PersonalityQuestionRepository;
import com.forysiak.personalitytest.domain.user.dao.UserDao;
import com.forysiak.personalitytest.domain.user.model.UserDocument;
import com.forysiak.personalitytest.domain.user.repository.UserRepository;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PersonalityTestServiceApplication.class)
public class PersonalityQuestionServiceTest {

  @Autowired
  private PersonalityQuestionService personalityQuestionService;

  @Autowired
  private PersonalityQuestionRepository personalityQuestionRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PersonalityQuestionDAO personalityQuestionDAO;

  @Autowired
  private UserDao userDao;

  @Before
  public final void setUp() {
    personalityQuestionRepository.deleteAll();
  }

  @After
  public final void tearDown() {

  }

  @Test
  public void should_properly_add_new_questions(){
    // given
    QuestionDTO[] correctQuestions = {
        QuestionDTO
            .builder()
            .category(CategoryType.HARD_FACT)
            .question("Is life good ?")
            .questionType(
                QuestionTypeDTO
                    .builder()
                    .type(AnswerType.SINGLE_CHOICE)
                    .options(Lists.newArrayList("First", "Second", "Third", "Fourth", "Fifth"))
                    .build()
            )
            .build(),
        QuestionDTO
            .builder()
            .category(CategoryType.PASSION)
            .question("Do you like sleeping ?")
            .questionType(
                QuestionTypeDTO
                    .builder()
                    .type(AnswerType.SINGLE_CHOICE_CONDITIONAL)
                    .options(Lists.newArrayList("First", "Second", "Third"))
                    .build()
            )
            .build()
    };

    //when
    personalityQuestionService.addNewQuestions(correctQuestions);

    // then
    List<PersonalityQuestionDocument> questions = personalityQuestionRepository.findAll();
    List<PersonalityQuestionDocument> hardFactQuestions = questions.stream().filter(
        question -> CategoryType.HARD_FACT.equals(question.getCategory())
    ).collect(Collectors.toList());
    List<PersonalityQuestionDocument> passionQuestions = questions.stream().filter(
        question -> CategoryType.PASSION.equals(question.getCategory())
    ).collect(Collectors.toList());
    List<PersonalityQuestionDocument> conditionalQuestions = questions.stream().filter(
        question -> AnswerType.SINGLE_CHOICE_CONDITIONAL.equals(question.getAnswerType())
    ).collect(Collectors.toList());

    assertThat(questions.size()).isEqualTo(2);
    assertThat(hardFactQuestions.size()).isEqualTo(1);
    assertThat(hardFactQuestions.get(0).getQuestion()).isEqualTo("Is life good ?");
    assertThat(hardFactQuestions.get(0).getOptions().size()).isEqualTo(5);

    assertThat(passionQuestions.size()).isEqualTo(1);

    assertThat(conditionalQuestions.size()).isEqualTo(1);

  }

  @Test
  public void should_thrown_constraint_violation_exception_when_addin_incomplete_questions(){
    // given
    QuestionDTO[] incompleteQuestions = {
        QuestionDTO
            .builder()
            .category(CategoryType.HARD_FACT)
            .build()
    };

    // when + then
    assertThatExceptionOfType(ConstraintViolationException.class)
        .isThrownBy(() -> personalityQuestionService.addNewQuestions(incompleteQuestions))
        .matches(e -> e.getConstraintViolations().size() == 2);

  }

  @Test
  public void should_properly_submit_personality_test(){
    // given
    userDao.setupInitialData();
    personalityQuestionDAO.setupInitialData();
    UserDocument user = userRepository.findAll().get(0);
    String userId = user.getId();

    PersonalityQuestionFormDTO answerCandidatesFirstRound = PersonalityQuestionFormDTO.builder()
        .userId(userId)
        .answers(Lists.newArrayList(
            Answer.builder().question("What is your mood ?").category(CategoryType.INTROVERSION).providedAnswer("Good").questionId("1").build(),
            Answer.builder().question("Is life good ?").category(CategoryType.PASSION).providedAnswer("It is").questionId("2").build()
        )).build();

    PersonalityQuestionFormDTO answerCandidatesSecondRound = PersonalityQuestionFormDTO.builder()
        .userId(userId)
        .answers(Lists.newArrayList(
            Answer.builder().question("What is your mood ?").category(CategoryType.INTROVERSION).providedAnswer("Not that good").questionId("1").build(),
            Answer.builder().question("Is life pleasant ?").category(CategoryType.HARD_FACT).providedAnswer("Maybe").questionId("3").build()
        )).build();

    // when - first round
    personalityQuestionService.submitPersonalityTest(answerCandidatesFirstRound);

    // then - first round
    UserDocument userAfterFirstRound = userRepository.findById(userId).get();
    List<Answer> answersFirstRound = userAfterFirstRound.getAnswerList();
    Answer firstAnswerRoundOne = answersFirstRound.get(0);
    Answer secondAnswerRoundOne = answersFirstRound.get(1);

    assertThat(answersFirstRound.size()).isEqualTo(2);
    assertThat(firstAnswerRoundOne.getQuestion()).isEqualTo("What is your mood ?");
    assertThat(firstAnswerRoundOne.getProvidedAnswer()).isEqualTo("Good");
    assertThat(secondAnswerRoundOne.getCategory()).isEqualTo(CategoryType.PASSION);
    assertThat(secondAnswerRoundOne.getQuestionId()).isEqualTo("2");

    // when - second round
    personalityQuestionService.submitPersonalityTest(answerCandidatesSecondRound);

    // then - second round
    UserDocument userAfterSecondRound = userRepository.findById(userId).get();
    List<Answer> answersSecondRound = userAfterSecondRound.getAnswerList();

    Answer firstAnswerRoundTwo = answersSecondRound.get(0);

    assertThat(answersSecondRound.size()).isEqualTo(3);
    assertThat(firstAnswerRoundTwo.getProvidedAnswer()).isEqualTo("Not that good");
  }

}
