package com.forysiak.personalitytest.domain.user.model;

import com.forysiak.personalitytest.PersonalityTestServiceApplication;
import com.forysiak.personalitytest.domain.personality_question.model.answer.Answer;
import com.forysiak.personalitytest.domain.personality_question.model.question.CategoryType;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PersonalityTestServiceApplication.class)
public class UserDocumentTest {

  @Test
  public void should_properly_update_users_answers(){
    // given
    List<Answer> existingAnswers = Lists.newArrayList(
      Answer.builder()
          .category(CategoryType.INTROVERSION)
          .questionId("1")
          .question("Is it ok being alone ?")
          .providedAnswer("No").build(),
        Answer.builder()
            .category(CategoryType.HARD_FACT)
            .questionId("5")
            .question("What is your favorite cuisine ?")
            .providedAnswer("French")
            .build()
    );

    List<Answer> newAnswers = Lists.newArrayList(
        Answer.builder()
            .category(CategoryType.INTROVERSION)
            .questionId("1")
            .question("Is it ok being alone ?")
            .providedAnswer("Yes")
        .build(),
        Answer.builder()
            .category(CategoryType.PASSION)
            .questionId("6")
            .question("Do you give up oftenly ?")
            .providedAnswer("Rarely")
            .build()
    );

    UserDocument userDocument = UserDocument.builder().name("John").answerList(existingAnswers).build();

    // when
    userDocument.updateAnswers(newAnswers);

    // then
    List<Answer> updatedAnswers = userDocument.getAnswerList();
    Answer changedAnswer = updatedAnswers.stream().filter(answer -> answer.getQuestionId().equals("1")).findFirst().get();

    assertThat(updatedAnswers.size()).isEqualTo(3);
    assertThat(changedAnswer.getProvidedAnswer()).isEqualTo("Yes");

  }

}
