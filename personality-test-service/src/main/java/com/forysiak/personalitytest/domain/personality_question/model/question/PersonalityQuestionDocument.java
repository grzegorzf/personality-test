package com.forysiak.personalitytest.domain.personality_question.model.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forysiak.personalitytest.domain.personality_question.model.answer.AnswerType;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.List;

@Document(collection = "PersonalityQuestion")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PersonalityQuestionDocument {

  @Id
  @JsonProperty("questionId")
  private String id;

  @JsonProperty
  private String question;

  @JsonProperty
  private CategoryType category;

  @JsonProperty
  private AnswerType answerType;

  @JsonProperty
  private List<String> options;


}
