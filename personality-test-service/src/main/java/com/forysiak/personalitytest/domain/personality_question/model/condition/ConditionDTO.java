package com.forysiak.personalitytest.domain.personality_question.model.condition;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forysiak.personalitytest.domain.personality_question.model.question.QuestionDTO;
import lombok.Data;

import java.io.Serializable;

@Data
public class ConditionDTO implements Serializable {

  @JsonProperty
  private PredicateDTO predicate;

  @JsonProperty("if_positive")
  private QuestionDTO conditionalQuestionIfPredictionMeet;

}
