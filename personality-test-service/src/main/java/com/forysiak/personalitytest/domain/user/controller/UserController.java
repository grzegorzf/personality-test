package com.forysiak.personalitytest.domain.user.controller;

import com.forysiak.personalitytest.domain.user.model.UserDTO;
import com.forysiak.personalitytest.domain.user.model.UserDocument;
import com.forysiak.personalitytest.domain.user.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@Slf4j
@AllArgsConstructor
public class UserController {

  private final UserService userService;

  @GetMapping(UserApi.USER_PATH)
  public ResponseEntity<List<UserDocument>> getAllUsers() {
    List<UserDocument> users = userService.findAll();
    return new ResponseEntity<>(users, HttpStatus.OK);
  }

  @PostMapping(UserApi.USER_PATH)
  public ResponseEntity<Void> addNewUser(@Valid @RequestBody UserDTO user) {
    log.info("Adding new user : {}", user);
    userService.addNewUser(user);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
