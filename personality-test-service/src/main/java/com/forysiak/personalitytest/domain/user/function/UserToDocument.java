package com.forysiak.personalitytest.domain.user.function;

import com.forysiak.personalitytest.domain.user.model.UserDTO;
import com.forysiak.personalitytest.domain.user.model.UserDocument;

import java.util.function.Function;

public interface UserToDocument extends Function<UserDTO, UserDocument> {
}
