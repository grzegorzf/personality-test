package com.forysiak.personalitytest.domain.personality_question.controller;

import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionDocument;
import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionFormDTO;
import com.forysiak.personalitytest.domain.personality_question.model.question.QuestionDTO;
import com.forysiak.personalitytest.domain.personality_question.service.PersonalityQuestionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@Slf4j
@AllArgsConstructor
public class PersonalityQuestionController {

  private final PersonalityQuestionService personalityQuestionService;

  @GetMapping(PersonalityQuestionApi.PERSONALITY_QUESTIONS_PATH)
  public ResponseEntity<List<PersonalityQuestionDocument>> getAllQuestions() {
    List<PersonalityQuestionDocument> questions = personalityQuestionService.findAll();
    return new ResponseEntity<>(questions, HttpStatus.OK);
  }

  @PostMapping(PersonalityQuestionApi.PERSONALITY_QUESTIONS_PATH)
  public ResponseEntity<Void> addNewQuestions(@Valid @RequestBody QuestionDTO[] questions) {
    personalityQuestionService.addNewQuestions(questions);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PutMapping(PersonalityQuestionApi.SUBMIT_PERSONALITY_TEST_PATH)
  public ResponseEntity<Void> submitTest(@Valid @RequestBody PersonalityQuestionFormDTO personalityQuestionFormDTO) {
    log.info("User has submitted personality test: {}", personalityQuestionFormDTO);
    personalityQuestionService.submitPersonalityTest(personalityQuestionFormDTO);
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
