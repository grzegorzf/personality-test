package com.forysiak.personalitytest.domain.personality_question.function;

import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionDocument;
import com.forysiak.personalitytest.domain.personality_question.model.question.QuestionDTO;

import java.util.function.Function;

public interface QuestionToDocument extends Function<QuestionDTO, PersonalityQuestionDocument> {

}
