package com.forysiak.personalitytest.domain.personality_question.dao;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.forysiak.personalitytest.domain.personality_question.function.QuestionToDocument;
import com.forysiak.personalitytest.domain.personality_question.model.InitialDataDTO;
import com.forysiak.personalitytest.domain.personality_question.model.question.QuestionDTO;
import com.forysiak.personalitytest.domain.personality_question.repository.PersonalityQuestionRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
@AllArgsConstructor
public class PersonalityQuestionDAO {

  private final PersonalityQuestionRepository personalityQuestionRepository;
  private final QuestionToDocument questionToDocumentMapper;
  private final Environment env;

  @EventListener({ContextRefreshedEvent.class})
  public void onContextCreated() {
    if (shouldDataBePurgedOnStart()){
      setupInitialData();
    }
  }

  private boolean shouldDataBePurgedOnStart(){
    return BooleanUtils.isTrue(Boolean.valueOf(env.getProperty("my.purgeDataOnStart", "false")));
  }

  public void setupInitialData() {
    TypeReference<InitialDataDTO> mapType = new TypeReference<InitialDataDTO>(){};
    ObjectMapper objectMapper = new ObjectMapper();
    InputStream inputStream = TypeReference.class.getResourceAsStream("/static/json/initialPersonalityQuestionsData.json");

    try {
      InitialDataDTO initialData = objectMapper.readValue(inputStream, mapType);
      List<QuestionDTO> questions = initialData.getQuestions();
      setupQuestions(questions);
      log.info("Initial Personality questions populated successfully. Size of added collection: {}", questions.size());
    } catch (IOException e){
      log.error("Initial Personality questions could not be populated due to: {}", e.getMessage());
    }
  }

  private void setupQuestions(List<QuestionDTO> questions) {
    personalityQuestionRepository.deleteAll();
    personalityQuestionRepository.insert(
        questions.stream()
          .map(questionToDocumentMapper)
          .collect(Collectors.toList())
    );
  }

}

