package com.forysiak.personalitytest.domain.personality_question.model.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forysiak.personalitytest.domain.personality_question.model.answer.AnswerType;
import com.forysiak.personalitytest.domain.personality_question.model.condition.ConditionDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuestionTypeDTO implements Serializable {

  @JsonProperty
  private AnswerType type;

  @JsonProperty
  private List<String> options;

  @JsonProperty
  private Range range;

  @JsonProperty
  private ConditionDTO condition;

}
