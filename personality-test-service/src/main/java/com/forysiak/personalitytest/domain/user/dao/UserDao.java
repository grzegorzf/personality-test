package com.forysiak.personalitytest.domain.user.dao;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.forysiak.personalitytest.domain.user.function.UserToDocument;
import com.forysiak.personalitytest.domain.user.model.UserDTO;
import com.forysiak.personalitytest.domain.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Component
public class UserDao {

  private final UserRepository userRepository;
  private final UserToDocument userToDocumentMapper;
  private final Environment env;

  @EventListener({ContextRefreshedEvent.class})
  public void onContextCreated() {
    if (shouldDataBePurgedOnStart()){
      setupInitialData();
    }
  }

  private boolean shouldDataBePurgedOnStart(){
    return BooleanUtils.isTrue(Boolean.valueOf(env.getProperty("my.purgeDataOnStart", "false")));
  }

  public void setupInitialData(){
    userRepository.deleteAll();
    ObjectMapper mapper = new ObjectMapper();
    TypeReference<List<UserDTO>> mapType = new TypeReference<List<UserDTO>>() {};
    InputStream is = TypeReference.class.getResourceAsStream("/static/json/initialUsersData.json");
    try {
      List<UserDTO> users = mapper.readValue(is, mapType);
      userRepository.insert(
          users.stream()
            .map(userToDocumentMapper)
            .collect(Collectors.toList())
      );
      log.info("Initial users populated successfully: {}", users.size());
    } catch (IOException e) {
      log.error("Initial users could not be populated due to: {}", e.getMessage());
    }
  }

}
