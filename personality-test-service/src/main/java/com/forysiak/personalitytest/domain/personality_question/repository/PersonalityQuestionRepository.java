package com.forysiak.personalitytest.domain.personality_question.repository;

import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalityQuestionRepository extends MongoRepository<PersonalityQuestionDocument, String> {

}
