package com.forysiak.personalitytest.domain.personality_question.model.condition;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Condition {
  @JsonProperty
  private Predicate predicate;
}
