package com.forysiak.personalitytest.domain.personality_question.service;

import com.forysiak.personalitytest.domain.personality_question.function.QuestionToDocument;
import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionDocument;
import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionFormDTO;
import com.forysiak.personalitytest.domain.personality_question.model.question.QuestionDTO;
import com.forysiak.personalitytest.domain.personality_question.repository.PersonalityQuestionRepository;
import com.forysiak.personalitytest.domain.user.model.UserDocument;
import com.forysiak.personalitytest.domain.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Validated
@Slf4j
@AllArgsConstructor
public class MongoPersonalityQuestionService implements PersonalityQuestionService {

  private final PersonalityQuestionRepository personalityQuestionRepository;
  private final UserRepository userRepository;

  private final QuestionToDocument questionToDocumentMapper;

  @Override
  public void addNewQuestions(QuestionDTO[] questions) {

    List<PersonalityQuestionDocument> questionDocuments = Stream.of(questions)
        .map(questionToDocumentMapper)
        .collect(Collectors.toList());
    personalityQuestionRepository.saveAll(questionDocuments);

  }

  @Override
  public void submitPersonalityTest(PersonalityQuestionFormDTO personalityQuestionForm) {
    Optional<UserDocument> userDocument = userRepository.findById(personalityQuestionForm.getUserId());
    userDocument.ifPresent(user -> {
      user.updateAnswers(personalityQuestionForm.getAnswers());
      userRepository.save(user);
    });
  }

  @Override
  public List<PersonalityQuestionDocument> findAll() {
    return personalityQuestionRepository.findAll();
  }

}
