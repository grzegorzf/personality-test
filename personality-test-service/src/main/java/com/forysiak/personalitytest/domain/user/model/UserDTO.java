package com.forysiak.personalitytest.domain.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable {

  @JsonProperty
  @Size(min=4, max = 50, message = "Username should have between 4 and 50 characters")
  @Pattern(regexp = "^[A-Za-z0-9]*$", message = "Only alphanumeric characters are allowed")
  private String name;

  @JsonIgnore
  private List<String> answers;

}
