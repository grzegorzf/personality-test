package com.forysiak.personalitytest.domain.personality_question.service;

import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionDocument;
import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionFormDTO;
import com.forysiak.personalitytest.domain.personality_question.model.question.QuestionDTO;

import javax.validation.Valid;
import java.util.List;


public interface PersonalityQuestionService {

  void addNewQuestions(@Valid QuestionDTO[] questions);

  void submitPersonalityTest(PersonalityQuestionFormDTO personalityQuestionForm);

  List<PersonalityQuestionDocument> findAll();

}
