package com.forysiak.personalitytest.domain.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forysiak.personalitytest.domain.personality_question.model.answer.Answer;
import com.google.common.collect.Lists;
import lombok.*;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.List;
import java.util.Optional;

@Document(collection = "User")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class UserDocument {

  @Id
  @JsonProperty("userId")
  private String id;

  @JsonProperty
  private String name;

  @JsonProperty
  private List<Answer> answerList = Lists.newArrayList();

  @Transient
  public void updateAnswers(List<Answer> answers) {
    answers.forEach(answer -> {
      Optional<Answer> existingAnswer = answerList.stream()
          .filter(a -> a.getQuestionId().equals(answer.getQuestionId()))
          .findFirst();
      if (existingAnswer.isPresent()) existingAnswer.get().setProvidedAnswer(answer.getProvidedAnswer());
      else  answerList.add(answer);
    });
  }
}
