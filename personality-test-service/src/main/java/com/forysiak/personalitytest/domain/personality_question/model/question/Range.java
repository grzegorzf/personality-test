package com.forysiak.personalitytest.domain.personality_question.model.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class Range implements Serializable {
  @JsonProperty
  private int from;

  @JsonProperty
  private int to;
}
