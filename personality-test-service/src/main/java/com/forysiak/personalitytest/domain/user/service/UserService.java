package com.forysiak.personalitytest.domain.user.service;

import com.forysiak.personalitytest.domain.user.model.UserDTO;
import com.forysiak.personalitytest.domain.user.model.UserDocument;

import java.util.List;

public interface UserService {

  List<UserDocument> findAll();

  UserDocument addNewUser(UserDTO user);

}
