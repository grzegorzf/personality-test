package com.forysiak.personalitytest.domain.personality_question.model.condition;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PredicateDTO implements Serializable {
  @JsonProperty
  private List<String> exactEquals;
}
