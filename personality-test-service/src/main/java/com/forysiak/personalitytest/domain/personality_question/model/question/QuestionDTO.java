package com.forysiak.personalitytest.domain.personality_question.model.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forysiak.personalitytest.domain.personality_question.model.condition.ConditionDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDTO implements Serializable {

  @NotNull
  @Valid
  @JsonProperty
  private String question;

  @NotNull
  @Valid
  @JsonProperty
  private CategoryType category;

  @NotNull
  @Valid
  @JsonProperty("question_type")
  private QuestionTypeDTO questionType;

  @JsonProperty
  private ConditionDTO condition;
}

