package com.forysiak.personalitytest.domain.personality_question.controller;

class PersonalityQuestionApi {
  static final String PERSONALITY_QUESTIONS_PATH = "/personality-questions";
  static final String SUBMIT_PERSONALITY_TEST_PATH = PERSONALITY_QUESTIONS_PATH + "/submit-test";
}
