package com.forysiak.personalitytest.domain.personality_question.model.answer;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum AnswerType {
  @JsonProperty("single_choice") SINGLE_CHOICE,
  @JsonProperty("number_range") NUMBER_CHOICE,
  @JsonProperty("single_choice_conditional") SINGLE_CHOICE_CONDITIONAL
}
