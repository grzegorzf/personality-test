package com.forysiak.personalitytest.domain.personality_question.function;

import com.forysiak.personalitytest.domain.personality_question.model.question.PersonalityQuestionDocument;
import com.forysiak.personalitytest.domain.personality_question.model.question.QuestionDTO;
import org.springframework.stereotype.Component;

@Component
public class QuestionDTOToPersonalityQuestionDocumentFunction implements QuestionToDocument  {
  @Override
  public PersonalityQuestionDocument apply(QuestionDTO question) {
    return PersonalityQuestionDocument.builder()
        .question(question.getQuestion())
        .category(question.getCategory())
        .answerType(question.getQuestionType().getType())
        .options(question.getQuestionType().getOptions())
        .build();

  }
}
