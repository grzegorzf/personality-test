package com.forysiak.personalitytest.domain.personality_question.model.answer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forysiak.personalitytest.domain.personality_question.model.question.CategoryType;
import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class Answer implements Serializable {

  @JsonProperty
  private String questionId;

  @JsonProperty
  private CategoryType category;

  @JsonProperty
  private String question;

  @JsonProperty
  private String providedAnswer;

}
