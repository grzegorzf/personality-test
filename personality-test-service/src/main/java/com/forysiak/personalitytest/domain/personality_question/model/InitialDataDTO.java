package com.forysiak.personalitytest.domain.personality_question.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forysiak.personalitytest.domain.personality_question.model.question.CategoryType;
import com.forysiak.personalitytest.domain.personality_question.model.question.QuestionDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class InitialDataDTO {

  @JsonProperty("categories")
  List<CategoryType> categories;

  @JsonProperty("questions")
  List<QuestionDTO> questions;

}
