package com.forysiak.personalitytest.domain.user.function;

import com.forysiak.personalitytest.domain.user.model.UserDTO;
import com.forysiak.personalitytest.domain.user.model.UserDocument;
import org.springframework.stereotype.Component;

@Component
public class UserDTOToUserDocumentFunction implements UserToDocument {
  @Override
  public UserDocument apply(UserDTO userDTO) {

    return UserDocument.builder()
      .name(userDTO.getName())
      .build();
  }

}
