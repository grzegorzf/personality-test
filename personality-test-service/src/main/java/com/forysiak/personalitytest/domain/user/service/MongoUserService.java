package com.forysiak.personalitytest.domain.user.service;

import com.forysiak.personalitytest.domain.user.function.UserToDocument;
import com.forysiak.personalitytest.domain.user.model.UserDTO;
import com.forysiak.personalitytest.domain.user.model.UserDocument;
import com.forysiak.personalitytest.domain.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class MongoUserService implements UserService {

  private final UserRepository userRepository;
  private final UserToDocument userToDocumentMapper;

  @Override
  public List<UserDocument> findAll() {
    return userRepository.findAll();
  }

  @Override
  public UserDocument addNewUser(UserDTO user) {
    return userRepository.save(userToDocumentMapper.apply(user));
  }
}
