package com.forysiak.personalitytest.domain.personality_question.model.question;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum CategoryType {
  @JsonProperty("hard_fact") HARD_FACT,
  @JsonProperty("lifestyle") LIFESTYLE,
  @JsonProperty("introversion") INTROVERSION,
  @JsonProperty("passion") PASSION
}
