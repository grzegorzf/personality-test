package com.forysiak.personalitytest.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties("my")
public class CustomApplicationProperties {
  private Boolean purgeDataOnStart;
}
