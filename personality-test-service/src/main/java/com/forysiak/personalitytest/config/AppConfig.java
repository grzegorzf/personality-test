package com.forysiak.personalitytest.config;

import com.forysiak.personalitytest.config.properties.CustomApplicationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@Configuration
@Import({MongoConfig.class, WebConfig.class})
@EnableConfigurationProperties(CustomApplicationProperties.class)
public class AppConfig {
  @Bean
  public MethodValidationPostProcessor methodValidationPostProcessor() {
    return new MethodValidationPostProcessor();
  }
}
