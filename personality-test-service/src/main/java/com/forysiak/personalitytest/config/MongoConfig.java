package com.forysiak.personalitytest.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages="com.forysiak.personalitytest.domain.*.repository")
@PropertySource("classpath:application.properties")
public class MongoConfig {

  @Value("${spring.data.mongodb.database}")
  private String databaseName;

  @Value("${spring.data.mongodb.host}")
  private String host;

  @Value("${spring.data.mongodb.port}")
  private int port;

  @Bean
  public Mongo mongo() {
    return new MongoClient(host, port);
  }

  @Bean
  public MongoTemplate mongoTemplate() {
    return new MongoTemplate((MongoClient) mongo(), databaseName);
  }

}
