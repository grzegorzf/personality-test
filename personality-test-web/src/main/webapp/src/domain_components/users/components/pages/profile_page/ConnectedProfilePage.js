import React from 'react'
import {connect} from 'react-redux'
import {ProfilePage} from "./ProfilePage";
import {selectSelectedUser, selectUsers, fetchUsersRequest, markUserAsSelected} from "../../../redux";

const mapStateToProps = state => (
    {
      selectedUser: selectSelectedUser(state),
      users: selectUsers(state)
    }
);

const mapDispatchToProps = dispatch => (
    {
      doFetchUsers: () => dispatch(fetchUsersRequest()),
      doMarkUserAsSelected: selectedUserId => dispatch(markUserAsSelected(selectedUserId))
    }
);

export const ConnectedProfilePage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfilePage);
