import React, {Fragment} from 'react'
import PropTypes from 'prop-types'
import {CTAButton, Form} from "../../../../../../ui_components";

export const AddUserForm = ({onChange, onClick, isFormReady}) => (
  <Form>
    <Fragment>
    <input name="name" onChange={onChange} />
    <CTAButton
      disabled={!isFormReady}
      value="Add new user"
      onClick={onClick}
    />
    </Fragment>
  </Form>
);

AddUserForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  isFormReady: PropTypes.bool.isRequired
};