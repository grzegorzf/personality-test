import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter()});
import {expect} from 'chai';
import {ProfilePage} from "./ProfilePage";
import {AnswerList} from "../../../../../ui_components/output/answer";

const answerList = [
  {questionId: "1", category: "lifestyle", providedAnswer: "Very often", question: "How often ?"},
  {questionId: "2", category: "hard_fact", providedAnswer: "Very far", question: "How far ?"}
];

const selectedUser = {name: "John", userId: "2", answerList};
const users = [selectedUser, {name: "Naty", userId: "1", answerList: []}];

const profilePagePropsForSelectedUser = {
  doFetchUsers: () => {},
  doMarkUserAsSelected:() => {},
  selectedUser,
  users
};

const profilePagePropsForNotSelectedUser = {
  doFetchUsers: () => {},
  doMarkUserAsSelected:() => {},
  selectedUser: null,
  users: []
};

describe('ProfilePage', () => {
  it('renders properly for selected user', () => {
    // given
    const MockedPage = <ProfilePage {...profilePagePropsForSelectedUser} />;
    // when
    const wrapper = shallow(MockedPage);
    // then
    expect(wrapper.html()).to.include('<span class="selected">John</span>');
    expect(wrapper.find(AnswerList)).to.have.length(1);
  });

  it('renders properly for not-selected user', () => {
    const MockedPage = <ProfilePage {...profilePagePropsForNotSelectedUser} />;
    // when
    const wrapper = shallow(MockedPage);
    // then
    expect(wrapper.find(AnswerList)).to.have.length(0);
    expect(wrapper.html()).to.include('user not selected');
  })

});