import React from 'react'
import {Headline, Page, Description} from "../../../../../ui_components";
import PropTypes from 'prop-types';

export class AddUserPage extends React.Component {

  state = {
    name: ""
  };

  render(){
    return (
        <Page>
          <Headline>Add new user</Headline>
          <Description>TODO - to be add later.</Description>
        </Page>
    )
  }
}

AddUserPage.propTypes = {
  questions: PropTypes.array,
  categories: PropTypes.array,
  fetchQuestions: PropTypes.func,
  fetchCategories: PropTypes.func
};