import React from 'react'
import {Description, Form, Headline, Page, Select, Selected} from "../../../../../ui_components";
import PropTypes from 'prop-types';
import {AnswerList} from "../../../../../ui_components";

export class ProfilePage extends React.Component {

  componentDidMount(){
    const {users, doFetchUsers} = this.props;
    if (!users || !users.length) doFetchUsers();
  }

  onChange = ({target: {name, value: selectedUserId}}) => {
    const {doMarkUserAsSelected} = this.props;
    doMarkUserAsSelected(selectedUserId);
  };

  render(){
    const {users, selectedUser} = this.props;
    const {onChange} = this;
    const userOptions = users.map(({userId, name}) => ({value: userId, label: name}));

    return (
      <Page>
        <Headline>Selected profile: <Selected>{selectedUser ? selectedUser.name : 'user not selected'}</Selected></Headline>
        <Description>Select active profile to fill personality test:</Description>
        <Form>
          <Select name="name" options={userOptions} onChange={onChange}/>
        </Form>
        {selectedUser && <AnswerList answers={selectedUser.answerList} />}
      </Page>
    )
  }
}

ProfilePage.propTypes = {
  users: PropTypes.array,
  selectedUser: PropTypes.object,
  doFetchUsers: PropTypes.func,
  doMarkUserAsSelected: PropTypes.func
};

