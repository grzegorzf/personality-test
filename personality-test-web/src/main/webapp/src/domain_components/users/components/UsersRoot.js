import React from 'react'
import {Route, Switch} from "react-router-dom";
import {ConnectedProfilePage, ConnectedAddUserPage} from './pages'

export const UsersRoot = ({match}) => (
  <Switch>
    <Route path={`${match.url}/add-user`} component={ConnectedAddUserPage} />
    <Route path={match.url} component={ConnectedProfilePage} />
  </Switch>
);