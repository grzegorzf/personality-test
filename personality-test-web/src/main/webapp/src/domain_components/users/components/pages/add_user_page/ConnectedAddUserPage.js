import {connect} from 'react-redux'
import {AddUserPage} from "./AddUserPage";
import {addUserRequest} from "../../../redux";

const mapStateToProps = state => (
    {

    }
);

const mapDispatchToProps = dispatch => (
    {
      addUser: () => dispatch(addUserRequest())
    }
);

export const ConnectedAddUserPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddUserPage);
