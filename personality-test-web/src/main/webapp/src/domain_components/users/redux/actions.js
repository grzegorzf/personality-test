import * as UsersActionType from './UsersActionType';

export const fetchUsersRequest = () => ({
  type: UsersActionType.FETCH_USERS_REQUEST
});

export const fetchUsersSuccess = users => ({
  type: UsersActionType.FETCH_USERS_SUCCESS,
  payload: {users}
});

export const fetchUsersFailure = reason => ({
  type: UsersActionType.FETCH_USERS_FAILURE,
  payload: {reason}
});

export const addUserRequest = userCandidate => ({
  type: UsersActionType.ADD_USER_REQUEST,
  payload: {userCandidate}
});

export const addUserSuccess = newUser => ({
  type: UsersActionType.ADD_USER_SUCCESS,
  payload: {newUser}
});

export const addUserFailure = reason => ({
  type: UsersActionType.ADD_USER_FAILURE,
  payload: {reason}
});

export const markUserAsSelected = selectedUserId => ({
  type: UsersActionType.MARK_USER_AS_SELECTED,
  payload: {selectedUserId}
});