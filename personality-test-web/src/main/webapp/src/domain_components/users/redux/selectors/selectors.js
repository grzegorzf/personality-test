export const selectSelectedUser = ({users: {users, selectedUserId}}) => users.filter(({userId}) => userId === selectedUserId)[0];
export const selectUsers = state => state.users.users;
export const selectSelectedUserId = state => state.users.selectedUserId;