import {usersReducer} from "../redux";
import {usersInitialState} from './initialState'
import * as UsersActionType from './UsersActionType';
import {expect} from 'chai';

describe('users_reducer', () => {

  it('properly store selectedUserId in state', () => {
    // given
    const selectedUserId = "2";
    const markUserAsSelectedAction = {type: UsersActionType.MARK_USER_AS_SELECTED, payload: {selectedUserId}};
    // when
    const result = usersReducer(usersInitialState, markUserAsSelectedAction);
    // then
    expect(result.selectedUserId).to.equal(selectedUserId);
  });


  it('properly store users in state', () => {
    // given
    const users = [{userId: "1", name: "Greg", answerList: []}, {userId: "2", name: "Naty", answerList: []}];
    const fetchUsersSuccessAction = {type: UsersActionType.FETCH_USERS_SUCCESS, payload: {users}};
    // when
    const result = usersReducer(usersInitialState, fetchUsersSuccessAction);
    // then
    expect(result.users).to.have.length("2");
  });

});