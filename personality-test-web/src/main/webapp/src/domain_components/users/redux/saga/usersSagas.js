import {call, put} from "redux-saga/effects";
import {usersClient} from '../../client';
import {
  addUserFailure,
  addUserSuccess,
  fetchUsersFailure,
  fetchUsersSuccess
} from "../actions";

export function* addUser(action) {
  const {payload: {userCandidate}} = action;
  try {
    const {data} = yield call(usersClient.addUser(userCandidate));
    yield put(addUserSuccess(data));
  } catch (e){
    yield put(addUserFailure(e.response));
  }
}

export function* fetchUsers() {
  try {
    const {data} = yield call(usersClient.fetchUsers);
    yield put(fetchUsersSuccess(data));
  } catch (e){
    yield put(fetchUsersFailure(e.response));
  }
}
