import {usersInitialState} from "./initialState";
import * as UsersActionType from './UsersActionType'

export const usersReducer = (state = usersInitialState, action) => {

  switch(action.type){
    case UsersActionType.MARK_USER_AS_SELECTED:
      const {payload: {selectedUserId}} = action;
      return {
          ...state,
        selectedUserId
      };

    case UsersActionType.FETCH_USERS_SUCCESS:
      const {payload: {users}} = action;
      return {
        ...state,
        users
      };

    case UsersActionType.ADD_USER_SUCCESS:
      const {payload: {newUser}} = action;
      return {
        ...state,
        users: [].concat(state.users, newUser)
      };

    default:
      return state;

  }

};