import * as UsersActionType from "../UsersActionType";
import {fetchUsers, addUser} from "./usersSagas";
import {takeLatest} from 'redux-saga/effects'

export function* usersSagaWatcher() {
  yield takeLatest(UsersActionType.FETCH_USERS_REQUEST, fetchUsers);
  yield takeLatest(UsersActionType.ADD_USER_REQUEST, addUser);
}