import axios from 'axios';

export const usersClient = {
  fetchUsers: () => axios.get("/api/users"),
  addUser: userCandidate => axios.post("/api/users", userCandidate)
};