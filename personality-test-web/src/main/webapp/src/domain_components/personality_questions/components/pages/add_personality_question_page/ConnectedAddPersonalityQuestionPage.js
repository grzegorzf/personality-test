import {connect} from 'react-redux'
import {AddPersonalityQuestionPage} from "./AddPersonalityQuestionPage";
import {fetchPersonalityQuestionsRequest, fetchPersonalityCategoriesRequest} from "../../../redux/actions";
import {selectCategories, selectQuestions} from "../../../redux/selectors";

const mapStateToProps = state => (
    {
      questions: selectQuestions(state),
      categories: selectCategories(state)
    }
);

const mapDispatchToProps = dispatch => (
    {
      fetchQuestions: () => dispatch(fetchPersonalityQuestionsRequest()),
      fetchCategories: () => dispatch(fetchPersonalityCategoriesRequest())
    }
);

export const ConnectedAddPersonalityQuestionPage = connect(
    mapStateToProps,
    mapDispatchToProps,
)(AddPersonalityQuestionPage);
