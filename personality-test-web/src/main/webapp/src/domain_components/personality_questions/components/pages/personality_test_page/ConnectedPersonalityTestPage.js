import {connect} from 'react-redux'
import {PersonalityTestPage} from "./PersonalityTestPage";
import {fetchPersonalityQuestionsRequest, submitPersonalityTestRequest} from "../../../redux/actions";
import {selectQuestions} from "../../../redux/selectors";
import {selectSelectedUser} from "../../../../users/redux";

const mapStateToProps = state => (
  {
    questions: selectQuestions(state),
    selectedUser: selectSelectedUser(state)
  }
);

const mapDispatchToProps = dispatch => (
  {
    doSubmitPersonalityTest: personalityTest => dispatch(submitPersonalityTestRequest(personalityTest)),
    doFetchQuestions: () => dispatch(fetchPersonalityQuestionsRequest())
  }
);

export const ConnectedPersonalityTestPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PersonalityTestPage);
