import React, {Fragment} from 'react'
import {CTAButton, Description, Headline, Page, QuestionList, Selected} from "../../../../../ui_components";
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'

export class PersonalityTestPage extends React.Component {

  state = {
    answers: [],
    questions: []
  };

  static getDerivedStateFromProps(props, state) {
    const {questions, selectedUser} = props;
    if (!state.questions.length && !!questions.length && !!selectedUser){
      return {
          ...state,
        questions: PersonalityTestPage.getQuestionsWithAnswersIfProvided(props, state)
      }
    }
    return state;
  }

  static getQuestionsWithAnswersIfProvided = ({questions, selectedUser}, {answers}) => {
    // Note to self: be careful because getQuestionsWithAnswersIfProvided tends to cache props
    const {answerList} = selectedUser;
    return questions.map(question => {

      let matchingExistingAnswer = answerList.filter(a => a.questionId === question.questionId)[0];
      let matchingAnswerCandidate = answers.filter(a => a.questionId === question.questionId)[0];

      if (matchingAnswerCandidate) question.providedAnswer = matchingAnswerCandidate.providedAnswer;
      else if (matchingExistingAnswer) question.providedAnswer = matchingExistingAnswer.providedAnswer;
      else question.providedAnswer = null;

      return question;
    });
  };

  componentDidMount(){
    const {questions, doFetchQuestions} = this.props;
    if (!questions || !questions.length) doFetchQuestions();
  }

  onChange = ({name: questionId, value: providedAnswer, category, question, answerType}) => {
    const {props} = this;
    const {answers} = this.state;
    answers.push({questionId, providedAnswer, category, question});
    this.setState({
      answers,
      questions: PersonalityTestPage.getQuestionsWithAnswersIfProvided(props, {answers})
    });
  };

  isReadyToSubmit = () => !!this.state.answers.length;

  onSubmit = () => {
    const {selectedUser: {userId}, doSubmitPersonalityTest} = this.props;
    const {answers} = this.state;
    doSubmitPersonalityTest({userId, answers})
  };

  renderUserNotSelectedInfo = () => {
    return (
      <Description>
        <Selected>Please select user on profile page first.</Selected>
      </Description>
    )
  };

  render(){
    const {selectedUser} = this.props;
    const {questions} = this.state;
    const {onChange, onSubmit, renderUserNotSelectedInfo} = this;
    const isReadyToSubmit = this.isReadyToSubmit();

    return (
      <Page>
        <Headline>Personality Test for <Selected>{selectedUser ? selectedUser.name : '- user not selected'}</Selected></Headline>
        <Description>Fill all of the questions to tell us more about your personality.</Description>
        {!selectedUser && renderUserNotSelectedInfo()}
        {selectedUser && (
            <Fragment>
              <QuestionList onChange={onChange} questions={questions} />
              <CTAButton disabled={!isReadyToSubmit} onClick={onSubmit} value="Submit your answers" />
            </Fragment>
        )}
      </Page>
    )
  }
}

PersonalityTestPage.propTypes = {
  selectedUser: PropTypes.object,
  questions: PropTypes.array.isRequired,
  doFetchQuestions: PropTypes.func.isRequired,
  doSubmitPersonalityTest: PropTypes.func.isRequired
};