import React from 'react'
import {Description, Headline, Page} from "../../../../../ui_components";
import {commonOnChange} from "../../../../../root_components/providers";

export class AddPersonalityQuestionPage extends React.Component {

  state = {

  };

  onChange = commonOnChange.bind(this);

  render(){
    return (
        <Page>
          <Headline>Add new personality question</Headline>
          <Description>TODO - to be added later.</Description>
        </Page>
    )
  }
}

AddPersonalityQuestionPage.propTypes = {

};