import React from 'react'
import {Route, Switch} from "react-router-dom";
import {ConnectedPersonalityTestPage, ConnectedAddPersonalityQuestionPage} from './pages'

export const PersonalityQuestionsRoot = ({match}) => (
  <Switch>
    <Route path={`${match.url}/add-question`} component={ConnectedAddPersonalityQuestionPage} />
    <Route path={match.url} component={ConnectedPersonalityTestPage} />
  </Switch>
);