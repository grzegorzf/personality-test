import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter()});
import {expect} from 'chai';
import {PersonalityTestPage} from "./PersonalityTestPage";
import {QuestionList} from "../../../../../ui_components/input/question";

const answerList = [
  {
    question: "How often ?",
    providedAnswer: "never",
    category: "lifestyle",
    questionId: "1"
  },
  {
    question: "How far ?",
    providedAnswer: "Very far",
    category: "hard_fact",
    questionId: "2"
  }
];
const selectedUser = {name: "Monique", userId: "2", answerList};
const users = [selectedUser, {name: "Naty", userId: "1", answerList: []}];


const propsWithQuestionsAndSelectedUser = {
  selectedUser: selectedUser,
  questions: [
    {
      question: "How often ?",
      providedAnswer: "Very often",
      category: "lifestyle",
      answerType: "single_choice",
      options: ["never", "once or twice a year", "socially", "frequently"],
      questionId: "1",
    }
  ],
  doFetchQuestions: () => {},
  doSubmitPersonalityTest: () => {}
};

const propsWithoutSelectedProfile = {
  selectedUser: null,
  questions: [],
  doFetchQuestions: () => {},
  doSubmitPersonalityTest: () => {}
};

describe('PersonalityTestPage', () => {

  it('properly renders QuestionList', () => {
    // given
    const MockedPage = <PersonalityTestPage {...propsWithQuestionsAndSelectedUser} />;
    // when
    const wrapper = shallow(MockedPage);
    // then
    expect(wrapper.find(QuestionList)).to.have.length(1);
    expect(wrapper.html()).to.contain('<span class="selected">Monique</span>');
    expect(wrapper.html()).to.contain('question answered');
  });

  it('properly renders info about not selected user', () => {
    // given
    const MockedPage = <PersonalityTestPage {...propsWithoutSelectedProfile} />;
    // when
    const wrapper = shallow(MockedPage);
    // then
    expect(wrapper.find(QuestionList)).to.have.length(0);
    expect(wrapper.html()).to.contain('user not selected');
  });

});