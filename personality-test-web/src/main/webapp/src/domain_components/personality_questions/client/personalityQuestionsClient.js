import axios from 'axios';

export const personalityQuestionsClient = {
  submitPersonalityTest: personalityTest => axios.put(`/api/personality-questions/submit-test`, personalityTest),
  fetchPersonalityQuestions: () => axios.get(`/api/personality-questions`),
  addPersonalityQuestion: questionCandidate => axios.post(`/api/personality-questions`, questionCandidate),
  fetchPersonalityCategories: () => axios.get(`/api/personality-categories`)
};