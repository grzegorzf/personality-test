import {personalityQuestionsReducer} from "../redux";
import {personalityQuestionsInitialState} from './initialState'
import * as PersonalityQuestionsActionType from './PersonalityQuestionsActionType';
import {expect} from 'chai';

const questions = [
  {
    question: "So?",
    category: "lifestyle",
    answerType: "single_choice",
    options: ["never", "once or twice a year", "socially", "frequently"],
    questionId: "1"
  },
  {
    question: "Well?",
    category: "lifestyle",
    answerType: "single_choice",
    options: ["never", "once or twice a year", "socially", "frequently"],
    questionId: "2"
  }
];

describe('personality_questions_reducer', () => {
  it('properly store personality questions in state', () => {
    // given
    const action = {type: PersonalityQuestionsActionType.FETCH_PERSONALITY_QUESTIONS_SUCCESS, payload: {questions}};
    // when
    const result = personalityQuestionsReducer(personalityQuestionsInitialState, action);
    // then
    expect(result.questions).to.have.length(2);
  })
});