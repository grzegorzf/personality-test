
export const selectQuestions = state => state.questions.questions;

export const selectCategories = state => state.questions.categories;