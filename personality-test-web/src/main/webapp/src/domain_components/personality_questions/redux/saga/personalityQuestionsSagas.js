import {call, put} from "redux-saga/effects";
import {
  addPersonalityQuestionFailure,
  addPersonalityQuestionSuccess,
  fetchPersonalityCategoriesFailure,
  fetchPersonalityCategoriesSuccess,
  fetchPersonalityQuestionsFailure,
  fetchPersonalityQuestionsSuccess, submitPersonalityTestFailure, submitPersonalityTestSuccess
} from "../actions";
import {personalityQuestionsClient} from '../../client';
import {fetchUsersRequest} from "../../../users/redux";

export function* fetchPersonalityQuestions() {
  try {
    const {data} = yield call(personalityQuestionsClient.fetchPersonalityQuestions);
    yield put(fetchPersonalityQuestionsSuccess(data));
  } catch (e){
    yield put(fetchPersonalityQuestionsFailure(e.response));
  }
}

export function* fetchPersonalityCategories() {
  try {
    const data = yield call(personalityQuestionsClient.fetchPersonalityCategories);
    yield put(fetchPersonalityCategoriesSuccess(data));
  } catch (e){
    yield put(fetchPersonalityCategoriesFailure(e.response));
  }
}

export function* addPersonalityQuestion(action) {
  try {
    const {payload: {questionCandidate}} = action;
    const {data} = yield call(personalityQuestionsClient.addPersonalityQuestion, questionCandidate);
    yield put(addPersonalityQuestionSuccess(data));
  } catch (e){
    yield put(addPersonalityQuestionFailure(e.response));
  }
}

export function* submitPersonalityTest(action) {
  try {
    const {payload: {personalityTest}} = action;
    yield call(personalityQuestionsClient.submitPersonalityTest, personalityTest);
    yield put(fetchUsersRequest());
    alert("You have succesfully submitted your answers.");
  } catch (e) {
    yield put(submitPersonalityTestFailure(e.response));
  }
}