import {personalityQuestionsInitialState} from './initialState'
import * as PersonalityQuestionsActionType from './PersonalityQuestionsActionType'

export const personalityQuestionsReducer = (state = personalityQuestionsInitialState, action) => {

  switch (action.type){
    case PersonalityQuestionsActionType.FETCH_PERSONALITY_QUESTIONS_SUCCESS:
      const {payload: {questions}} = action;
      return {
          ...state,
        questions
      };

    default:
      return state;
  }

};