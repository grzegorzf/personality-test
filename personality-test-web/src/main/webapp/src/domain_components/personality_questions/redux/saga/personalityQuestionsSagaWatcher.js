import {takeLatest} from 'redux-saga/effects'
import * as PersonalityQuestionsActionType from "../PersonalityQuestionsActionType";
import {
  fetchPersonalityQuestions,
  fetchPersonalityCategories,
  addPersonalityQuestion, submitPersonalityTest
} from "./personalityQuestionsSagas";

export function* personalityQuestionsSagaWatcher() {
  yield takeLatest(PersonalityQuestionsActionType.FETCH_PERSONALITY_QUESTIONS_REQUEST, fetchPersonalityQuestions);
  yield takeLatest(PersonalityQuestionsActionType.ADD_PERSONALITY_QUESTION_REQUEST, addPersonalityQuestion);
  yield takeLatest(PersonalityQuestionsActionType.FETCH_PERSONALITY_CATEGORIES_REQUEST, fetchPersonalityCategories);
  yield takeLatest(PersonalityQuestionsActionType.SUBMIT_PERSONALITY_TEST_REQUEST, submitPersonalityTest);
}