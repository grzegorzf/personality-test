import * as PersonalityQuestionsActionType from './PersonalityQuestionsActionType'

export function fetchPersonalityQuestionsRequest() {
  return {
    type: PersonalityQuestionsActionType.FETCH_PERSONALITY_QUESTIONS_REQUEST
  };
}

export function fetchPersonalityQuestionsSuccess(questions) {
  return {
    type: PersonalityQuestionsActionType.FETCH_PERSONALITY_QUESTIONS_SUCCESS,
    payload: {questions}
  };
}

export function fetchPersonalityQuestionsFailure(reason) {
  return {
    type: PersonalityQuestionsActionType.FETCH_PERSONALITY_QUESTIONS_FAILURE,
    payload: {reason}
  };
}

export function fetchPersonalityCategoriesRequest() {
  return {
    type: PersonalityQuestionsActionType.FETCH_PERSONALITY_CATEGORIES_REQUEST
  };
}

export function fetchPersonalityCategoriesSuccess(categories) {
  return {
    type: PersonalityQuestionsActionType.FETCH_PERSONALITY_CATEGORIES_SUCCESS,
    payload: {categories}
  };
}

export function fetchPersonalityCategoriesFailure(reason) {
  return {
    type: PersonalityQuestionsActionType.FETCH_PERSONALITY_CATEGORIES_FAILURE,
    payload: {reason}
  };
}

export function addPersonalityQuestionRequest(questionCandidate) {
  return {
    type: PersonalityQuestionsActionType.ADD_PERSONALITY_QUESTION_REQUEST,
    payload: {questionCandidate}
  };
}

export function addPersonalityQuestionSuccess(newQuestion) {
  return {
    type: PersonalityQuestionsActionType.ADD_PERSONALITY_QUESTION_SUCCESS,
    payload: {newQuestion}
  };
}

export function addPersonalityQuestionFailure(reason) {
  return {
    type: PersonalityQuestionsActionType.ADD_PERSONALITY_QUESTION_FAILURE,
    payload: {reason}
  };
}

export function submitPersonalityTestRequest(personalityTest) {
  return {
    type: PersonalityQuestionsActionType.SUBMIT_PERSONALITY_TEST_REQUEST,
    payload: {personalityTest}
  };
}

export function submitPersonalityTestSuccess(updatedProfile) {
  return {
    type: PersonalityQuestionsActionType.SUBMIT_PERSONALITY_TEST_SUCCESS,
    payload: {updatedProfile}
  };
}

export function submitPersonalityTestFailure(reason) {
  return {
    type: PersonalityQuestionsActionType.SUBMIT_PERSONALITY_TEST_FAILURE,
    payload: {reason}
  };
}