import React, {Component, Fragment} from 'react';
import {StyledApp, Header} from "../../../ui_components/layout";
import {Route, Switch} from "react-router-dom";
import { NavLink } from "react-router-dom";
import {UsersRoot} from './../../../domain_components/users'
import {PersonalityQuestionsRoot} from './../../../domain_components/personality_questions'
import {Menu} from "../menu";

export class App extends Component {
  render() {
    return (
      <StyledApp>
        <Header>
          <Menu />
        </Header>
        <Switch>
          <Route path="/test" component={PersonalityQuestionsRoot}/>
          <Route path="/profile" component={UsersRoot}/>
          <Route path="/" component={PersonalityQuestionsRoot}/>
        </Switch>
      </StyledApp>
    );
  }
}