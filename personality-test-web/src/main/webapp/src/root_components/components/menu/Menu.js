import { NavLink } from "react-router-dom";
import React, {Fragment} from 'react';

export const Menu = () => (
  <Fragment>
    <NavLink exact activeClassName="active" to="/profile">Profile</NavLink>
    <NavLink exact activeClassName="active" to="/test">Personality Test</NavLink>
    <NavLink exact activeClassName="active" to="/test/add-question">Add question</NavLink>
    <NavLink exact activeClassName="active" to="/profile/add-user">Add user</NavLink>
  </Fragment>
);
