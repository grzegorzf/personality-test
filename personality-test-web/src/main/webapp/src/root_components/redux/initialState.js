import {personalityQuestionsInitialState as questions} from "../../domain_components/personality_questions/redux";
import {usersInitialState as users} from "../../domain_components/users/redux";

export const initialState = {
  questions,
  users
};