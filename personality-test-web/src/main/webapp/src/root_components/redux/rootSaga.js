import {all} from 'redux-saga/effects'
import {personalityQuestionsSagaWatcher, usersSagaWatcher} from "../../domain_components"

export const rootSaga = function* () {
  yield all([
    personalityQuestionsSagaWatcher(),
    usersSagaWatcher()
  ]);
};