import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';
import {
  personalityQuestionsReducer as questions,
  usersReducer as users
} from "./../../domain_components";

export const rootReducer = combineReducers({
  routing: routerReducer,
  questions,
  users
});
