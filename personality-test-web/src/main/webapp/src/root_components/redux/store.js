import { createStore} from 'redux'
import {rootReducer} from "./rootReducer";
import {initialState} from "./initialState";
import {rootSaga} from "./rootSaga";
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware} from 'react-router-redux'
import {applyMiddleware, compose} from "redux";
import createSagaMiddleware from "redux-saga";

export const history = createHistory();

// 1. Create enhancers
const enhancers = [];
const sagaMiddleware = createSagaMiddleware();

const middleware = [
  sagaMiddleware,
  routerMiddleware(history)
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

// 2. Boot up a store

export const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
);

sagaMiddleware.run(rootSaga);
