import React from 'react';
import './css/question_list.css'
import PropTypes from 'prop-types';
import {Question} from './Question'

export const QuestionList = ({questions, onChange}) => (
  <div className="question-list">
    {questions.map(question => <Question key={question.questionId} {...question} onChange={onChange} />)}
  </div>
);

QuestionList.propTypes = {
  questions: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired
};