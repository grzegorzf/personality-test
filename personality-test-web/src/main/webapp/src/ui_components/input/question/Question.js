import React from 'react';
import './css/question.css'
import PropTypes from 'prop-types';
import {Select} from "../select";

export const Question = ({question, category, providedAnswer, options, answerType, questionId: name, onChange}) => {
  const hasAnswerBeenProvided = !!providedAnswer;
  const preparedOptions = options.map(option => ({value: option, label: option} ));
  const onAnswerChange = ({target: {name, value}}) => {
    onChange({name, value, category, question, answerType});
  };

  return (
    <section className={`question${hasAnswerBeenProvided ? ' answered' : ' not-answered'}`}>
      <p>{question} <small>({answerType})</small></p>
      <Select
        sort={false}
        value={providedAnswer}
        onChange={onAnswerChange}
        name={name}
        options={preparedOptions}
      />
    </section>
  )
};

Question.propTypes = {
  providedAnswer:PropTypes.string,
  questionId: PropTypes.string.isRequired,
  question: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  answerType: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

