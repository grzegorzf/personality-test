import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {expect} from 'chai';
import {Question} from "./Question";
import {Select} from "../select";

Enzyme.configure({ adapter: new Adapter()});

const questionProps = {
  question: "How often do you smoke?",
  providedAnswer: "Dummy answer",
  category: "lifestyle",
  answerType: "single_choice",
  options: ["never", "once or twice a year", "socially", "frequently"],
  questionId: "5b3e4b9329f296237cbced12",
  onChange: () => {}
};

describe('Question', () => {
  it('properly renders provided options', () => {
    //given
    const MockedQuestion = <Question {...questionProps} />;
    // when
    const wrapper = shallow(MockedQuestion);
    // then
    expect(wrapper.find(Select)).to.have.length(1);
    expect(wrapper.find('section.answered')).to.have.length(1);
  });
});