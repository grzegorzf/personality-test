import React from 'react';
import './css/select.css'
import PropTypes from 'prop-types';

export const Select = ({value: providedValue, options, onChange, name, sort = true}) => {
  if (sort) sortOptions(options);
  const preparedOptions = options.map(({value, label}, index) => <option key={index} value={value}>{label}</option>);
  return (
      <select defaultValue={providedValue} onChange={onChange} name={name}>
        <option selected disabled>Please select one</option>
        {preparedOptions}
      </select>
  )
};

const sortOptions = options => {
  options.sort(({label: l1}, {label: l2}) => {
    if (l1 < l2) return -1;
    if (l1 > l2) return 1;
    return 0;
  });
};

Select.propTypes = {
  sort: PropTypes.bool,
  name: PropTypes.any.isRequired,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired
};