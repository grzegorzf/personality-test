import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter()});
import {expect} from 'chai';
import {Select} from "../select";

const selectProps = {
  sort: true,
  name: 'test',
  options: [{label: "Lost", value: "lost"}, {label:"First", value: "first"}, {label:"Second", value: "second"}, {label: "Third", value: "third"}],
  providedValue: "second",
  onChange: () => {}
};

describe('Select', () => {
  it('properly renders Select', () => {
    //given
    const MockedSelect = <Select {...selectProps} />;
    // when
    const wrapper = shallow(MockedSelect);
    // then
    expect(wrapper.find('option')).to.have.length(5);
    expect(wrapper.find('option').at(1).text()).to.equal("First");
    expect(wrapper.html()).to.contain('name="test"');
  });

});