import React from 'react';
import './css/button.css'
import PropTypes from 'prop-types'

export const CTAButton = ({onClick, value, disabled = false}) => (
  <button
    disabled={disabled}
    type="button"
    onClick={onClick}
    value={value}
    className="cta-button">
    {value}
  </button>
);

CTAButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};