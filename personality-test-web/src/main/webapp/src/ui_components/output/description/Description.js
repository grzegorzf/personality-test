import React from 'react';
import './css/description.css'

export const Description = ({children}) => (
    <p className="description">{children}</p>
);