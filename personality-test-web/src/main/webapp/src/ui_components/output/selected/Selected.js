import React from 'react';
import './css/selected.css'

export const Selected = ({children}) => (
  <span className="selected">
    {children}
  </span>
);