import React from 'react';
import './css/answer.css'
import PropTypes from 'prop-types';
import {Answer} from "./Answer";

export const AnswerList = ({answers}) => (
  <div className="answer-list">
    {answers.map(answer => <Answer key={answer.questionId} {...answer}/>)}
  </div>
);

AnswerList.propTypes = {
  answers:PropTypes.array.isRequired
};