import React from 'react';
import './css/answer.css'
import PropTypes from 'prop-types';

export const Answer = ({question, category, providedAnswer}) => (
  <section className="answer">
    <p>{question}</p>
    <b>{providedAnswer}</b>
  </section>
);

Answer.propTypes = {
  question: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  providedAnswer: PropTypes.string.isRequired,
};
