import React from 'react';
import './css/page.css'

export const Page = ({children}) => (
  <div className="page">
    {children}
  </div>
);