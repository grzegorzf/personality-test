import React from 'react';
import './css/headline.css'

export const Headline = ({children}) => (
  <h2>{children}</h2>
);