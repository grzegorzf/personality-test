import React from 'react';
import './css/app.css'

export const StyledApp = ({children}) => (
  <div className="app">
    {children}
  </div>
);
