import React from 'react';
import './css/header.css'

export const Header = ({children}) => (
    <header>
      <nav>
        {children}
      </nav>
    </header>
);
