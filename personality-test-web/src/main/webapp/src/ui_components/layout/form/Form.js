import React from 'react'
import './css/form.css'

export const Form = ({children}) => (
    <form>
      {children}
    </form>
);
