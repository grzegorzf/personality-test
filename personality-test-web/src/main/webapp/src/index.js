import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import {store, history} from "./root_components/redux";
import {App} from './root_components'
import registerServiceWorker from './registerServiceWorker';

render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();