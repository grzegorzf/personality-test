const path = require('path');

module.exports = {
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  entry: [
    'react-hot-loader/patch', // activate HMR for React
    'webpack-dev-server/client?http://localhost:8080',// bundle the client for webpack-dev-server and connect to the provided endpoint
    'webpack/hot/only-dev-server', // bundle the client for hot reloading, only- means to only hot reload for successful updates
    './src/index.js' // the entry point of our app
  ],
  devServer: {
    port: 8888,
    historyApiFallback: true,
    contentBase: './',
    hot: true,
    proxy: {
      "/api/*": "http://localhost:8080"
    }
  }
};