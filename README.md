#### About

This is small full-stack coding exercise using Spring, MongoDB and React.

#### Instruction

JSON file located in data/reference_data.json contains schema of personality questions, that should be presented in form, submitted and assigned and persisted to a given account.


#### How to run back-end

Since it's a Spring Boot Application, run PersonalityTestService to start backend on default port of 8080. MongoDB is used as a DB solution - make sure it's running locally on default port 27017 (unless you want to configure it through application properties).

#### How to run front-end

Front-end app is located in personality-test-web/src/main/webapp.

Since for productivity reasons it's based on <b>create-react-app</b>, type 'yarn start' (or 'npm run start') to run app app on default address of localhost:3000. 

Type 'yarn test' (or respectively 'npm run test') to run tests.

#### Status of completion

Saga's tests for front-end and Controller and DAO tests for back-end would be a nice addition (both will be eventually added). 

Also, some additional features such as an ability to add new users and questions have been started but not completed.